#include "shell.hh"

int main()
{ std::string input;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
  std::string prompt = ">>> ";

  std::string arg = "";

  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file(){ // ToDo: Implementeer volgens specificatie.
   const char* bestands_naam[] = {NULL};
   const char* invoer[] = {NULL};

   std::string tmp_naam;
   std::string tmp_invoer;
   std::string text_invoer;

   std::cout << "Geef de gewenste bestandsnaam:" << std::endl;
   std::getline(std::cin, tmp_naam);

   bestands_naam[0] = tmp_naam.c_str();

   std::cout << "Geef een invoer voor in het bestand:" << std::endl;
   std::string tmp_tmp_invoer;

   while (std::getline(std::cin, tmp_invoer)){
      if (tmp_invoer == "<EOF>"){
         break;
      }
         tmp_tmp_invoer += tmp_invoer + "\n";
      }
      invoer[0] = tmp_tmp_invoer.c_str();
      int fd = syscall(SYS_creat, bestands_naam[0], 0644);
      syscall(SYS_write, fd, invoer[0], tmp_tmp_invoer.size());
}

void list() // ToDo: Implementeer volgens specificatie.
{
   int pid = syscall(SYS_fork);
   if (pid == 0) {
      const char* arguments[] = {"/usr/bin/ls", "-l", "-a", NULL};
      syscall(SYS_execve, arguments[0], arguments, NULL);
   } else {
      syscall(SYS_wait4, pid, NULL, NULL, NULL);
   }
}

void find() // ToDo: Implementeer volgens specificatie.
{ 
std::string usr_rqst;
std::cout << "What do you wish to zoek? " << std::endl;
std::getline(std::cin, usr_rqst);

int fd[2];
int pid0 = syscall(SYS_fork);


if (pid0 == 0) {
   const char* arguments[] = {"/usr/bin/find", ".", NULL};
   syscall(SYS_dup2, fd[1], 1);
   syscall(SYS_execve, arguments[0], arguments, NULL);
   exit(0);
}

int pid1 = syscall(SYS_fork);

if (pid1 == 0) {
   const char* arguments[] = {"/usr/bin/grep", usr_rqst.c_str(), NULL};
   syscall(SYS_close, fd[1]);
   syscall(SYS_dup2, fd[0], 0);
   syscall(SYS_execve, arguments[0], arguments[1], NULL);
   exit(0);
}

syscall(SYS_close, fd[0]);
syscall(SYS_wait4, pid0, NULL, NULL, NULL);
syscall(SYS_wait4, pid1, NULL, NULL, NULL);

}

void seek() // ToDo: Implementeer volgens specificatie.
{
int fdSeek = syscall(SYS_creat, "seek", 0644);
int fdLoop = syscall(SYS_creat, "loop", 0644);

syscall(SYS_write, fdSeek, "x", 1);
syscall(SYS_lseek, fdSeek, 5000000, SEEK_END);
syscall(SYS_write, fdSeek, "x", 1);

syscall(SYS_write, fdLoop, "x", 1);
for (int i = 0; i=5000000; i++) {
   syscall(SYS_write, fdLoop, "\0", 1);
}
syscall(SYS_write, fdLoop, "x", 1);

syscall(SYS_close, fdSeek);
syscall(SYS_close, fdLoop);

}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
