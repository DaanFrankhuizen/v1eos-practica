#include <iostream>
#include <string>
using namespace std;


int count_chars(string zin, string inputKarakter) {
	int count = 0;

	for (uint i = 0; i < zin.size(); i++)
		if (zin[i] == inputKarakter[0]) {
			count++;};
	
	cout << "COUNTERPOUNTER: " << count << endl;
	
	return count;
}



int main(int num_args, char** arg_char) {
	string inputKarakter;
	string zin = "";

	if (arg_char[1] == "help") {
		cout << "./opdracht0 -s [SENTENCE HERE] -c [CHARACTER HERE]" << endl;
	} else if (arg_char[1] == "s") {
		zin = arg_char[2];
		if (arg_char[3] == "c") {
			char* char_c = arg_char[4];
			string str(char_c);
			inputKarakter = char_c;
			count_chars(zin, inputKarakter);
			return 0;
		} else {
			cerr << "You have not given a correct argument";
			return -1;
		}
	}

	return 0;
}
